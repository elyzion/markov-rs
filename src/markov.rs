#[macro_use] extern crate log;
extern crate env_logger;
#[macro_use] extern crate lazy_static;
extern crate regex;
extern crate rand;

use std::collections::*;
use std::fmt;
use std::error::Error;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use std::io::{self, BufReader};
use std::result::Result;
use regex::Regex;
use rand::{thread_rng, Rng};
use std::string::*;
use std::ops::*;

const NOWORD: &'static str = "\n";

#[derive(Debug)]
struct Markov {
    n: u8,
    max_gen: usize,
    min_gen: usize,
    table: HashMap<String, Vec<String>>, //String will work for character based generator
    // need a pattern database of pos to characters, in addition to standard. 
    // can then filter the Vec<String> from table using the character list for a given pos.
    // this will allow us to avoid things like -'s on the ends of names.
    // Ideally this would actually store vowel, consonant and whitespace occurances, not actual
    // characters. This will allow for more freedom in word formation.
    position_data: HashMap<usize, String>
}

impl Markov {
    fn new(n: u8, max_gen: usize, min_gen: usize) -> Markov {
        Markov {
            n: n,
            max_gen: max_gen,
            min_gen: min_gen,
            table: HashMap::new(),
            position_data: HashMap::new()
        }
    }

    fn build(& mut self) {
        let f = File::open("assets/utf8.txt").unwrap();
        let mut reader = BufReader::new(f);
        let mut buffer = String::new();
        let word_re: Regex = Regex::new(r"^\s*(?P<word>[^\s]+)\s*$").unwrap();
        let char_re: Regex = Regex::new(r"(?P<char>.)").unwrap();

        while reader.read_line(&mut buffer).unwrap() > 0 {
            for capture in word_re.captures_iter(buffer.as_str()) {
                let mut pos: usize = 0;
                let mut w1 = NOWORD;
                let mut w2 = NOWORD;
                // not changing matched stuff to lower case atm.
                let captured: &str = capture.name("word").unwrap().as_str();

                if captured.len() < self.min_gen {
                    self.min_gen = captured.len();
                    debug!("Adjusting min_gen to {:?}", self.min_gen);
                } else if captured.len() > self.max_gen {
                    self.max_gen = captured.len();
                    debug!("Adjusting max_gen to {:?}", self.max_gen);
                }

                debug!("captured {:?}\n", captured);
                for letter in char_re.captures_iter(captured) {

                    let character: &str = letter.name("char").unwrap().as_str();
                    debug!("character {:?}\n", character);
                    let key = [w1, w2].join(" ");
                    debug!("key {:?}", key);
                    debug!("table {:?}", self.table);
                    debug!("position_data {:?}", self.position_data);

                    if !self.position_data.contains_key(&pos) {
                        self.position_data.insert(pos, String::from(character));
                    } else if let Some(v) = self.position_data.get_mut(&pos) { 
                        if !v.contains(character) {
                            v.push_str(character);
                        }
                    }

                    self.table.
                        entry(key).
                        or_insert(Vec::new()).
                        push(String::from(character));
                    w1 = w2;
                    w2 = character;
                    pos = pos + 1;
                }
            }
            buffer.clear()
        }
    }

    fn generate(&self) -> Result<String, String> {
        let mut word = String::new();
        let mut w1 = String::from(NOWORD);
        let mut w2 = String::from(NOWORD);
        let mut rand = thread_rng();

        for i in 0..self.max_gen {
            let key = [w1.clone(), w2.clone()].join(" ");
            debug!("Built key '{:?}'", key);
            match self.table.get(&key) {
                None => {
                    debug!("No list found for key '{:?}', terminal reached", key);
                    if i >= self.min_gen {
                        debug!("This terminal is a standard terminal");
                        break;
                    } else if i < self.min_gen {
                        return Err("Dead end reached, abnormal terminal".to_string())
                    }
                }
                Some(list) => {
                    let filtered = self.position_data.get(&i).map_or(vec![String::new()], |allowed_characters| {
                        debug!("i: {:?} \nallowed_chars: {:?} \nlist: {:?}", i, allowed_characters, list);
                        (0 .. list.len()).
                            filter(|&list_pos| allowed_characters.contains(&list[list_pos])).
                            //fold(String::new(), |mut filter, list_pos| { 
                            //    filter.push_str(&list[list_pos]);
                            //    filter
                            //})
                            map(|list_pos| list[list_pos].clone()).
                            collect::<Vec<String>>()
                    });

                    debug!("filtered: {:?}", filtered);
                    if filtered.len() > 0 {
                        let index = rand.gen_range(0, filtered.len());
                        let next_character = filtered[index].clone();
                        debug!("Index {:?} \nLen {:?} \nnext_word {:?}\n", index, filtered.len(), next_character);
                        word.push_str(&next_character.clone());
                        w1 = w2;
                        w2 = next_character.clone();
                    } else if filtered.len() == 0 && word.len() >= self.min_gen {
                        debug!("No allowed characters -- standard terminal");
                        break;
                    } else {
                        return Err(format!("Empty list for key {:?}", key))
                            //panic!("Empty list for key".to_string())
                    }
                }
            };

            if word.len() >= self.max_gen {
                debug!("Reached word length terminal paranoia check -- ");
                break;
            }

        }
        println!("{}", word);
        Ok(word)
    }

}


fn main() {
    env_logger::init();
    let mut m = Markov::new(1u8, 6, 2);
    m.build();
    for i in 1..100 {
        m.generate();
    }

}

#[cfg(test)]
mod tests {

    use super::Markov;
    #[test]
    fn generate() {
        let mut m = Markov::new(1u8, 6, 2);
        assert!(m.table.capacity() >= 0);
        m.build();
        let word = m.generate().unwrap();
    }
    #[test]
    fn build() {
        let mut m = Markov::new(1u8, 6, 2);
        m.build()
    }
}
